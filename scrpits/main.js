new Vue({
  el: "#app",
  data: {
    menusGlobal: [
      { text: "Sketch", icon: "pencil-alt" },
      { text: "Add", icon: "plus-square" },
      { text: "Transform", icon: "cube" },
      { text: "Tools", icon: "hammer" },
      /*
      { text: "sketch", icon: "draw-polygon" },
      { text: "extrude", icon: "dice-d6" },
      { text: "extrude", icon: "shapes" },
      { text: "dimensions", icon: "ruler-combined" },
      */
      { text: "Delete", icon: "trash-alt",disable:true }
    ],
    contextualMenusByContext:{
      "": [],
      "Sketch": [
        { text: "Parrallel", icon: "parrallel" },
        /*{ text: "Perpendicular", icon: "perpendicular" },*/
        { text: "Perpendicular", icon: "tenge" },
        { text: "Tangent", icon: "voicemail" },
        /*{ text: "Tangent", icon: "tangent" },*/
        { text: "Concentric", icon: "dot-circle" },
        { text: "Equals", icon: "equals" },
        { text: "Symetry", icon: "symetry" },
        { text: "Disconnect", icon: "disconnect" },
        { text: "Lock", icon: "lock" }

      ]
    }
  },
  computed: {
    // a computed getter
    menusFonctions: function () {
      // `this` points to the vm instance
      return this.menusGlobal
      },
    menusContextual: function () {
      // `this` points to the vm instance
      return this.contextualMenusByContext["Sketch"]
      }
  },  
  methods: {
  	toggle: function(todo){
    	todo.done = !todo.done
    }
  }
})